#include <QtGui>

#include "renderarea.h"
#include "packager.h"

RenderArea::RenderArea(QWidget *parent)
    : QWidget(parent)
{

    setBackgroundRole(QPalette::Base);
    setAutoFillBackground(true);

    packager.init();
    packager.SetAlgorithm(&nextFit);
    //packager.UseAlgorithm();
}

QSize RenderArea::minimumSizeHint() const
{
    return QSize(STRIPW / 4, STRIPH / 4);
}
QSize RenderArea::sizeHint() const
{
    return QSize(STRIPW, STRIPH);
}
void RenderArea::SetHW(int h, int w)
{
    STRIPW = w;
    STRIPH = h;
}
void RenderArea::clear()
{
    packager._rectangles.clear();
    packager.rectangles.clear();
}
void RenderArea::addRect(QRect r)
{
    packager._rectangles.push_back(r);
}
void RenderArea::paintEvent(QPaintEvent * /* event */)
{
    QPainter painter(this);
    painter.setPen(QPen(QBrush(Qt::lightGray, Qt::SolidPattern), 3.0, Qt::DotLine));

    painter.save();
    painter.drawRect(10, 0, STRIPW, STRIPH - 10);
    painter.restore();

    painter.setPen(QPen(Qt::black));

    int sumSpace = 0;
    int minY = STRIPH;
    int fits = 0;
    int unfits = 0;
    for (int i = 0; i < packager.getSize(); i++) {
        //qDebug("y:%d",packager.rectangles[i].y());
        //qDebug("h:%d",packager.rectangles[i].height());
        if (packager.rectangles[i].y()>0) fits+=1; else unfits+=1;
        if (packager.rectangles[i].y() < minY) minY = packager.rectangles[i].y();
        sumSpace += packager.rectangles[i].height() * packager.rectangles[i].width();
        painter.setBrush(QBrush(QColor(255, 200, 100).lighter(120 - 8 * i),
                                Qt::SolidPattern));
        painter.save();
        painter.drawRect(packager.rectangles[i].translated(QPoint(10,-10)));
        painter.restore();
    }
    qDebug("optimal = %d, obtained = %d", sumSpace / STRIPW, STRIPH - minY);
    qDebug("fits = %d unfits:%d", fits, unfits);

}

void RenderArea::setAlg(int number)
{
    switch (number) {
    case 1:  packager.SetAlgorithm(&nextFit);       break;
    case 2:  packager.SetAlgorithm(&firstFit);      break;
    case 3:  packager.SetAlgorithm(&bestFit);       break;
    case 4:  packager.SetAlgorithm(&knapsack);      break;
    case 5:  packager.SetAlgorithm(&splitFit);      break;
    case 6:  packager.SetAlgorithm(&join);          break;
    case 7:  packager.SetAlgorithm(&floorCeil);     break;
    case 8:  packager.SetAlgorithm(&sleator);       break;
    case 9:  packager.SetAlgorithm(&burke);         break;
    case 10: packager.SetAlgorithm(&nextFitLevel);  break;
    case 11: packager.SetAlgorithm(&firstFitLevel); break;
    case 12: packager.SetAlgorithm(&bestFitLevel);  break;
    case 13: packager.SetAlgorithm(&biLevel);       break;
    case 14: packager.SetAlgorithm(&nextFitShelf);  break;
    case 15: packager.SetAlgorithm(&firstFitShelf); break;
    case 16: packager.SetAlgorithm(&bestFitShelf);  break;
    case 17: packager.SetAlgorithm(&harmonicShelf); break;
    case 18: packager.SetAlgorithm(&azar);          break;
    case 19: packager.SetAlgorithm(&compression);   break;
    case 20: packager.SetAlgorithm(&comprPartFit);  break;
    case 21: packager.SetAlgorithm(&comprFullFit);  break;
    case 22: packager.SetAlgorithm(&comprCombo);    break;
    case 23: packager.SetAlgorithm(&onlineFit);     break;
    default: break;
    }
    packager.UseAlgorithm();
}
