#ifndef PACKEGER_H
#define PACKEGER_H

#include <QRect>
#include <QList>
#include <QMap>
#include <QColor>

class Algorithm
{
public:
    Algorithm(){}

    virtual const QList<QRect> pack(const QList<QRect> rects){}
    virtual void setParam(qreal param) {}
};

///////////////////////////////////////////////////////////////////////////////
//  Offline algorithms
///////////////////////////////////////////////////////////////////////////////
class NFDH: public Algorithm
{
public:
    NFDH(){}

    const QList<QRect> pack(const QList<QRect> rects);
};

class FFDH: public Algorithm
{
public:
    FFDH(){}

    const QList<QRect> pack(const QList<QRect> rects);
};

class BFDH: public Algorithm
{
public:
    BFDH(){}

    const QList<QRect> pack(const QList<QRect> rects);
};

class KP01: public Algorithm
{
public:
    KP01(){}

    const QList<QRect> pack(const QList<QRect> rects);
};

class SF: public Algorithm
{
public:
    SF(){}

    const QList<QRect> pack(const QList<QRect> rects);

private:
    typedef struct sf {
        QRect rect;
        qreal scaledWidth;
        bool packedFirst;
    } Sf;
};

class JOIN: public Algorithm
{
public:
    JOIN(){_gamma = 0.05;}

    const QList<QRect> pack(const QList<QRect> rects);
    void setParam(qreal param) {_gamma = param;}

private:
    qreal _gamma;
};

class FCNR: public Algorithm
{
public:
    FCNR(){}

    const QList<QRect> pack(const QList<QRect> rects);
};

class Sleator: public Algorithm
{
public:
    Sleator(){}

    const QList<QRect> pack(const QList<QRect> rects);
};

class Burke: public Algorithm
{
public:
    Burke(){}

    const QList<QRect> pack(const QList<QRect> rects);
};
///////////////////////////////////////////////////////////////////////////////
//  Online algorithms
///////////////////////////////////////////////////////////////////////////////
class NFL: public Algorithm
{
public:
    NFL(){}

    const QList<QRect> pack(const QList<QRect> rects);
};

class FFL: public Algorithm
{
public:
    FFL(){}

    const QList<QRect> pack(const QList<QRect> rects);
};

class BFL: public Algorithm
{
public:
    BFL(){}

    const QList<QRect> pack(const QList<QRect> rects);
};

class BiNFL: public Algorithm
{
public:
    BiNFL(){}

    const QList<QRect> pack(const QList<QRect> rects);
};

class NFS: public Algorithm
{
public:
    NFS(){_base = static_cast<qreal>(0.7);}

    const QList<QRect> pack(const QList<QRect> rects);
    void setParam(qreal param) {_base = param;}

private:
    qreal _base;
};

class FFS: public Algorithm
{
public:
    FFS(){_base = static_cast<qreal>(0.7);}

    const QList<QRect> pack(const QList<QRect> rects);
    void setParam(qreal param) {_base = param;}

private:
    qreal _base;
};

class BFS: public Algorithm
{
public:
    BFS(){_base = static_cast<qreal>(0.7);}

    const QList<QRect> pack(const QList<QRect> rects);
    void setParam(qreal param) {_base = param;}

private:
    qreal _base;
};

class HS: public Algorithm
{
public:
    HS(){_base = static_cast<qreal>(0.7);}

    const QList<QRect> pack(const QList<QRect> rects);
    void setParam(qreal param) {_base = param;}

private:
    qreal _base;
};

class Azar: public Algorithm
{
public:
    Azar(){_gamma = 0.4;}

    const QList<QRect> pack(const QList<QRect> rects);
    void setParam(qreal param) {_gamma = param;}

private:
    qreal _gamma;
};

class CA: public Algorithm
{
public:
    CA(){}

    const QList<QRect> pack(const QList<QRect> rects);
};

class CPF: public Algorithm
{
public:
    CPF(){}

    const QList<QRect> pack(const QList<QRect> rects);
};

class CFF: public Algorithm
{
public:
    CFF(){}

    const QList<QRect> pack(const QList<QRect> rects);
};

class CC: public Algorithm
{
public:
    CC(){}

    const QList<QRect> pack(const QList<QRect> rects);
};

class OF: public Algorithm
{
public:
    OF(){}

    const QList<QRect> pack(const QList<QRect> rects);
private:
    int searchMinGap(const QList<int> gap, int width);
    int _level;
};
///////////////////////////////////////////////////////
class Packager
{
protected:
    Algorithm* algorithm;

private:


public:
    Packager(void){}

    void init();
    int getSize(void);
    void UseAlgorithm(void);
    void SetAlgorithm(Algorithm* a);
    void SetParameter(qreal param) {algorithm->setParam(param);}
    QList<QRect> _rectangles;
    QList<QRect> rectangles;

    class Level
    {
    public:
        Level(int b, int h, int f=0, int w=0) : bottom(b),
                                                height(h),
                                                floor(f),
                                                initW(w),
                                                ceiling(0){}

        const QRect put(const QRect &rect, bool f=true, bool leftJustified=true);
        bool ceilingFeasible(const QRect &rect, const QList<QRect> existing);
        bool floorFeasible(const QRect &rect);
        int getSpace(bool f=true);

        int bottom;
        int height;
        int floor;
        int initW;
        int ceiling;
    };
};
#endif // PACKEGER_H
