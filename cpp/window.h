#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>

QT_BEGIN_NAMESPACE
class QCheckBox;
class QComboBox;
class QLabel;
class QGridLayout;
class RenderArea;
class QLineEdit;
class QPushButton;
QT_END_NAMESPACE

class Window : public QWidget
{
    Q_OBJECT

public:
    Window();

private slots:
    void algChanged();
    void raHWchanged();
    void addRect();
    void addRandomRect();
    void clearRects();

private:
    RenderArea *renderArea;
    QLabel *algLabel;
    QComboBox *algComboBox;

    QLabel *areaLabel;
    QLineEdit *areaW;
    QLineEdit *areaH;
    QPushButton *updateArea;

    QLabel *newLabel;
    QLineEdit *newW;
    QLineEdit *newH;
    QPushButton *addButton;
    QPushButton *addrandButton;
    QPushButton *clearButton;

};

#endif // WINDOW_H
