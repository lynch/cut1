#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <QBrush>
#include <QPen>
#include <QPixmap>
#include <QWidget>

#include "packager.h"

class RenderArea : public QWidget
{
    Q_OBJECT

public:
    static int STRIPH;
    static int STRIPW;
    RenderArea(QWidget *parent = 0);
    void clear();
    void SetHW(int h, int w);
    void addRect(QRect r);
    QSize minimumSizeHint() const;
    QSize sizeHint() const;
    void setParameter(qreal param) {packager.SetParameter(param);}
    void reuseAlg() {packager.UseAlgorithm();}

protected:
    void paintEvent(QPaintEvent *event);

public slots:
    void setAlg(int number);

private:
    QPixmap pixmap;
    Packager packager;
    // offline
    NFDH    nextFit;
    FFDH    firstFit;
    BFDH    bestFit;
    KP01    knapsack;
    SF      splitFit;
    JOIN    join;
    FCNR    floorCeil;
    Sleator sleator;
    Burke   burke;
    // online
    NFL   nextFitLevel;
    FFL   firstFitLevel;
    BFL   bestFitLevel;
    BiNFL biLevel;
    NFS   nextFitShelf;
    FFS   firstFitShelf;
    BFS   bestFitShelf;
    HS    harmonicShelf;
    Azar  azar;
    CA    compression;
    CPF   comprPartFit;
    CFF   comprFullFit;
    CC    comprCombo;
    OF    onlineFit;
};

#endif // RENDERAREA_H
