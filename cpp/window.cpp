#include <QtGui>
#include <QtWidgets>


#include "renderarea.h"
#include "packager.h"
#include "window.h"

const int IdRole = Qt::UserRole;

Window::Window()
{
    renderArea = new RenderArea(this);
    renderArea->SetHW(500, 286);
    algComboBox = new QComboBox;
    algComboBox->addItem(tr("NFDH"),    1);
    algComboBox->addItem(tr("FFDH"),    2);
    algComboBox->addItem(tr("BFDH"),    3);
    //algComboBox->addItem(tr("KP01"),    4);
    //algComboBox->addItem(tr("SF"),      5);
    algComboBox->addItem(tr("JOIN"),    6);
    algComboBox->addItem(tr("FCNR"),    7);
    //algComboBox->addItem(tr("Sleator"), 8);
    algComboBox->addItem(tr("Burke"),   9);

    algLabel = new QLabel(tr("&Algorithm:"));
    algLabel->setBuddy(algComboBox);

    connect(algComboBox, SIGNAL(activated(int)),
            this, SLOT(algChanged()));

    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->setColumnStretch(0, 1);
    mainLayout->setColumnStretch(3, 1);
    mainLayout->addWidget(renderArea, 0, 0, 1, 4);
    mainLayout->addWidget(algLabel, 2, 0, Qt::AlignRight);
    mainLayout->addWidget(algComboBox, 2, 1);

    areaW = new QLineEdit("286");
    areaH = new QLineEdit("500");
    updateArea = new QPushButton("update");
    areaLabel = new QLabel("area");
    mainLayout->addWidget(areaLabel, 3, 0);
    mainLayout->addWidget(areaW, 3, 1);
    mainLayout->addWidget(areaH, 3, 2);
    mainLayout->addWidget(updateArea, 3, 3);

    connect(updateArea, SIGNAL(clicked()),
            this, SLOT(raHWchanged()));

    newW = new QLineEdit("10");
    newH = new QLineEdit("20");
    addButton = new QPushButton("add");
    newLabel = new QLabel("new rect");
    mainLayout->addWidget(newLabel, 4, 0);
    mainLayout->addWidget(newW, 4, 1);
    mainLayout->addWidget(newH, 4, 2);
    mainLayout->addWidget(addButton, 4, 3);

    connect(addButton, SIGNAL(clicked()),
            this, SLOT(addRect()));

    addrandButton = new QPushButton("addRandom");
    mainLayout->addWidget(addrandButton, 5, 3);
    connect(addrandButton, SIGNAL(clicked()),
            this, SLOT(addRandomRect()));

    clearButton = new QPushButton("clear");
    mainLayout->addWidget(clearButton, 6, 3);
    connect(clearButton, SIGNAL(clicked()),
            this, SLOT(clearRects()));

    setLayout(mainLayout);
    setWindowTitle(tr("2D Strip Packing demo"));
}

void Window::clearRects()
{
    renderArea->clear();

    /*QRect rect;
    rect.setRect(0, 0, 0, 0);
    renderArea->addRect(rect);
*/
  //  algChanged();
}
void Window::addRandomRect()
{
    QRect rect;
    rect.setRect(0, 0, qrand() % ((100 + 1) - 10) + 10,qrand() % ((100 + 1) - 10) + 10 );
    renderArea->addRect(rect);
    algChanged();
}
void Window::addRect()
{
    QRect rect;
    rect.setRect(0, 0, newW->text().toInt(), newH->text().toInt());
    renderArea->addRect(rect);
    algChanged();
}
void Window::raHWchanged()
{
    int w = areaW->text().toInt();
    int h = areaH->text().toInt();
    renderArea->SetHW(h,w);
    algChanged();
}
void Window::algChanged()
{
    int number = int(algComboBox->itemData(
                         algComboBox->currentIndex(),
                         IdRole).toInt());
    renderArea->setAlg(number);
    renderArea->update();
}



